word_frequency = Hash.new(0)

sentence = "The Wind and the Willows"
sentence.split.each do |word|
  word_frequency[word.downcase] += 1
end

puts word_frequency